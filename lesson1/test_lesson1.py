import unittest
from .lesson1 import *

class MyTestCase(unittest.TestCase):


    def test_minutes_to_seconds(self):
        self.assertEqual(minutes_to_seconds(3), 180)

    def test_hours_to_seconds(self):
        self.assertEqual(hours_to_seconds(2), 7200)

    def test_add_one(self):
        self.assertEqual(hours_to_seconds(5), 6)

    def test_addition(self):
        self.assertEqual(addition(3, 2), 5)

    def test_is_automorphic(self):
        self.assertEqual(is_automorphic(111), False)
        self.assertEqual(is_automorphic(625), True)

    def test_football_points(self):
        self.assertEqual(football_points(3, 4, 2), 13)
        self.assertEqual(football_points(5, 0, 2), 15)
        self.assertEqual(football_points(0, 0, 1), 0)

    def test_factorial(self):
        self.assertEqual(factorial(3), 6)
        self.assertEqual(factorial(5), 120)

    def test_calculator(self):
        self.assertEqual(calculator(5, "+", 3), 8)
        self.assertEqual(calculator(0, "*", 3), 0)
        self.assertEqual(calculator(4, "/", 2), 2)
        self.assertEqual(calculator(0, "-", 1), -1)

    def test_get_distance(self):
        self.assertEqual(get_distance((10, -5), (8, 16)), 21.095023109728988)

    def test_get_first_value(self):
        self.assertEqual(get_first_value([10, 20, 30]), 10)
        self.assertEqual(get_first_value([60, 20, 30]), 60)
        self.assertEqual(get_first_value([-1000, 200, 800]), -1000)

    def test_get_last_value(self):
        self.assertEqual(get_last_value([10, 20, 30]), 30)
        self.assertEqual(get_last_value([60, 20, 30]), 30)
        self.assertEqual(get_last_value([-1000, 200, 800]), 800)

    def test_get_smallest_value(self):
        self.assertEqual(get_smallest_value([10, 20, -4, 999, 1000]), -4)
        self.assertEqual(get_smallest_value([60, 500, 2, 30, 900]), 2)
        self.assertEqual(get_smallest_value([-1000, 200, 800]), -1000)

    def test_get_biggest_value(self):
        self.assertEqual(get_smallest_value([10, 20, -4, 999, 1000]), -4)
        self.assertEqual(get_smallest_value([60, 500, 2, 30, 900]), 2)
        self.assertEqual(get_smallest_value([-1000, 200, 800]), -1000)


if __name__ == '__main__':
    unittest.main()
