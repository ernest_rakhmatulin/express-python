# 1.1 Алгебраические задачи

def minutes_to_seconds(minutes):
    """
    Напишите функцию, которая принимает целочисленное значение
    (количество минут) и конвертирует это значение в секунды.

    Сложность: Очень легко

    Примечания:
    В минуте 60 секунд.

    Пример:
    minutes_to_seconds(5) -> 300
    minutes_to_seconds(3) -> 180
    minutes_to_seconds(2) -> 120
    """
    pass


def hours_to_seconds(hours):
    """
    Напишите функцию, которая принимает целочисленное значение
    (количество часов) и конвертирует это значение в секунды.

    Сложность: Очень легко

    Примечания:
    В минуте 60 секунд, в часе 60 минут.

    Пример:
    hours_to_seconds(2) -> 7200
    hours_to_seconds(10) -> 36000
    hours_to_seconds(24) -> 86400
    """
    pass


def add_one(number):
    """
    Напишите функцию, которая принимает число в качестве аргумента,
    увеличивает это число на 1 и возвращает результат.

    Сложность: Очень легко

    Пример:
    add_one(5) -> 6
    add_one(-100) -> -99
    add_one(999) -> 1000
    """
    pass


def addition(a, b):
    """
    Создайте функцию, которая принимает два числа в
    качестве аргументов и возвращает их сумму.

    Сложность: Очень легко

    Пример:
    addition(3, 2) -> 5
    addition(-3, -6) -> -9
    addition(7, 3) -> 10
    """
    pass

def is_automorphic(number):
    """
    Напишите функцию, которая принимает число и возвращает
    True если число автоморфное и False если не автоморфное

    Сложность: Очень легко

    Примечания:
    Автоморфное число — число, десятичная запись квадрата которого
    оканчивается цифрами самого этого числа.
    Например, число 5 в квадрате 25, 6 к квадрате 36, 625 в квадрате = 390625

    Пример:
    is_automorphic(5) -> True
    is_automorphic(21) -> False
    is_automorphic(625) -> True

    """
    pass

def football_points(wins, draws, losses):
    """
    Напишите функцию, которая принимает количество
    побед (wins), ничьих (draws) и проигрышей (losses)
    и рассчитывает количество очков, полученных футбольной
    командой на данный момент.

    Сложность: Очень легко

    Примечания:
    Победа - 3 очка, ничья - 1 очко, проигрыш - 0 очков.

    Пример:
    football_points(3, 4, 2) -> 13
    football_points(5, 0, 2) -> 15
    football_points(0, 0, 1) -> 0
    """
    pass


def factorial(number):
    """
    Напишите функцию, которая принимает целое число
    и возвращает факториал этого числа.

    Сложность: Легко

    Примечания:
    Факториал числа — это произведение натуральных чисел
    от 1 до самого числа (включая данное число).

    Пример:
    factorial(3) -> 6
    factorial(5) -> 120
    factorial(13) -> 6227020800
    """
    pass

def calculator(number_one, operator, number_two):
    """
    Напишите функцию, которая принимает два числа и математический оператор + - / *
    и выполнит вычисление с заданными числами.

    Сложность: Легко

    Примечания:
    Математический оператор передается в качесве строки "+", "-", "*", "/".
    Если на вход в качестве делителя (number_two) подается 0 вернуть сообщение "Нельзя делить на 0!".

    Пример:
    calculator(5, "+", 3) -> 8
    calculator(0, "*", 2) -> 0
    calculator(4, "/", 2) -> 2

    """
    pass

def get_distance(a, b):
    """
    Напишите функцию, которая рассчитывает расстояние между двумя точками.
    На вход подаются два кортежа каждый состоит из двух чисел (значений по координате x и y).
    Функция возвращает длинну отрезка проведенного через эти точки.

    Сложность: Легко

    Пример:
    get_distance((10, -5), (8, 16)) -> 21.095
    get_distance((0, 0), (1, 1)) -> 1.414
    get_distance((-2, 1), (4, 3)) -> 6.325

    """
    pass

# 1.2 Задачи со списками


def get_first_value(numbers_list):
    """
    Напишите функцию, которая принимает список чисел,
    и возвращает первый элемент этого списка.

    Сложность: Легко

    Примечания:
    Первый элемент списка имеет нулевой индекс.

    Пример:
    get_first_value([10, 20, 30]) -> 10
    get_first_value([1, 100, 400]) -> 1
    get_first_value([-1000, 200, 800]) -> -1000

    """
    pass

def get_last_value(numbers_list):
    """
    Напишите функцию, которая принимает список чисел,
    и возвращает последний элемент этого списка.

    Сложность: Легко

    Примечания:
    Первый элемент списка имеет индекс -1.

    Пример:
    get_last_value([10, 20, 30]) -> 30
    get_last_value([1, 100, 400]) -> 400
    get_last_value([-1000, 200, 800]) -> 800

    """
    pass


def get_smallest_value(numbers_list):
    """
    Напишите функцию, которая принимает список чисел,
    и возвращает самый маленький элемент этого списка.

    Сложность: Легко

    Пример:
    get_smallest_value([10, 20, 0]) -> 0
    get_smallest_value([999, 100, 400]) -> 100
    get_smallest_value([-1000, 200, 800]) -> -1000

    """
    pass

def get_biggest_value(numbers_list):
    """
    Напишите функцию, которая принимает список чисел,
    и возвращает самый большой элемент этого списка.

    Сложность: Легко

    Пример:
    get_biggest_value([10, 20, 0]) -> 20
    get_biggest_value([999, 100, 400]) -> 999
    get_biggest_value([-1000, 200, 800]) -> 800

    """
    pass