class Square:
    """
    Данный класс позвоялет создать объект квадрат.
    Конструктор принимает длину стороны в качестве параметра.

    В классе реализованы методы для нахождения площади и периметра.
    """
    def __init__(self, side):
        self.side = side

    def get_area(self):
        return self.side ** 2

    def get_perimeter(self):
        return 4 * self.side


class Rectangle:
    """
    Это класс прямоугольника. Реализуйте методы класса,
    для нахождения площиди и периметра.

    Сложность: Очень легко

    Пример:
    r = Rectangle(5, 9)
    r.get_area() => 45
    r.get_perimeter() => 28
    """

    def __init__(self, side_a, side_b):
        self.side_a = side_a
        self.side_b = side_b

    def get_area(self):
        pass

    def get_perimeter(self):
        pass


class Circle:
    """
    Реализуйте класс, который позволит создавать объект круга,
    конструктор в качестве параметра должен принимать радиус.

    Также необходимо реализовать методы для нахождения площади
    и периметра круга.

    Сложность: Легко

    Примечание:
    Число Пи = 3.14

    Пример:
    circle_1 = Circle(11)
    circle_1.get_area() => 379.94
    circle_2 = Circle(4.44)
    circle_2.get_perimeter() => 27.883200000000002
    """
    pass


class Employee:
    """
    Реализуйте класс сотрудник. Конструктор должен принимать
    следующие параметры: имя сотрудника, фамилию, а также адрес
    сайта компании, в которой работает сотрудник.

    Необходимо реализовать отдельные методы для установки
    и отображения электронной почты.

    Если имейл не был установлен вручную, то функция для
    отображения имейла предложит имейл на основании имени,
    фамилии и сайта компании.

    А также метод для отображения полного имени сотрудника.

    Сложность: Легко

    Пример:
    e = Employee("John", "Smith", "google.com")

    e.get_fullname() => "John Smith"
    e.get_email() => "john.smith@google.com"
    e.set_email("js@example.com")
    e.get_email() => "john.smith@google.com"
    e.get_email() => "john.smith@google.com"
    """
    pass


