import unittest
from .lesson_oop import *

class MyTestCase(unittest.TestCase):

    def test_square(self):
        s = Square(5)
        self.assertEqual(s.get_area(), 25)
        self.assertEqual(s.get_perimeter(), 20)

    def test_rectangle(self):
        s = Rectangle(5, 9)
        self.assertEqual(s.get_area(), 45)
        self.assertEqual(s.get_perimeter(), 28)

    def test_circle(self):
        circle_1 = Circle(11)
        circle_2 = Circle(4.44)
        self.assertEqual(circle_1.get_area(),  379.94)
        self.assertEqual(circle_2.get_perimeter(), 27.883200000000002)

    def test_employee(self):
        e = Employee("John", "Smith", "google.com")
        self.assertEqual(e.get_email(), "john.smith@google.com")
        e.set_email("js@example.com")
        self.assertEqual(e.get_email(), "js@example.com")
        self.assertEqual(e.get_email(), "js@example.com")
        self.assertEqual(e.get_fullname(), "John Smith")

if __name__ == '__main__':
    unittest.main()
