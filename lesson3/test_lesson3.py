import unittest
from .lesson3 import summa_treh_naibolshih_elementov, \
    rectangle, triangle, circle, element_less_5, elements_in_both_lists

class MyTestCase(unittest.TestCase):

    def test_summa_treh_naibolshih_elementov(self):
        self.assertEqual(summa_treh_naibolshih_elementov([1, 2, 3, 65, 234, 1, 534, 6, 12, 3]), 833)
        self.assertEqual(summa_treh_naibolshih_elementov([1, 2]), None)

    def test_element_less_5(self):
        self.assertEqual(element_less_5([1, 2, 3, 10, 99, 50, 2, 0, -1, 1000]), [1, 2, 3, 2, 0, -1])


    def test_rectangle(self):
        self.assertEqual(rectangle(4, 2), 8)

    def test_triangle(self):
        self.assertEqual(triangle(4, 10), 20)

    def test_circle(self):
        self.assertEqual(circle(10), 314)

    def test_elements_in_both_lists(self):
        self.assertEqual(elements_in_both_lists([1, 2, 3, 10], [2, 3, 4, 5, 10]), [2, 3, 10])

if __name__ == '__main__':
    unittest.main()
